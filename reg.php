<?php include "path.php"; 
    include "app/controllers/users.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>
<?php include("app/include/header.php"); ?>
<main>
<div class="container reg_form">
    <form action="reg.php" class="row justify-content-center" method="post" >
        <h2>Регистрация</h2>
        <div class="mb-3 col-12 col-md-4 err">
        <p><?php include 'app/helps/errorInfo.php'; ?></p>
        </div>
        <div class="w-100"></div>
        <div class="mb-3 col-12 col-md-4 labels">
            <label for="formGroupExampleInput" class="form-label">Ваше Имя</label>
            <input name="login" type="text" valuse="<?=$login?>" class="form-control" id="formGroupExampleInput" placeholder="Введите ваше имя...">
        </div>
        <div class="w-100"></div>
        <div class="mb-3 col-12 col-md-4 labels">
            <label for="exampleInputEmail1" class="form-label">Ваша почта</label>
            <input name="email" type="email" valuse="<?=$mail?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите вашу почту...">
            <div id="emailHelp" class="form-text">Ваша почта хранится только у нас.</div>
        </div>
        <div class="w-100"></div>
        <div class="mb-3 col-12 col-md-4 labels">
            <label for="exampleInputPassword1" class="form-label">Введите пароль</label>
            <input name="password-f" type="password" class="form-control" id="exampleInputPassword1">
        </div>
        <div class="w-100"></div>
        <div class="mb-3 col-12 col-md-4 labels">
            <label for="exampleInputPassword2" class="form-label">Повторите введенный пароль</label>
            <input name="password-s" type="password" class="form-control" id="exampleInputPassword2">
        </div>
        <div class="w-100"></div>
        <div class="mb-3 col-12 col-md-4 labels">
            <button type="submit" class="button" name="button-reg">Зарегистрироваться</button>
            <a href="<?php echo BASE_URL . 'login.php'; ?>" class="link-reg">Авторизироваться</a>
        </div>
    </form>
</div>
<?php include("app/include/footer.php"); ?>
</main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
