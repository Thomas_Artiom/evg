﻿
<?php 
    include 'path.php'; 
    include "app/controllers/topics.php";
    
    $page = isset($_GET['page']) ? $_GET['page']: 1;
    $limit = 20;
    $offset = $limit * ($page - 1);
    $total_pages = round(countRow('posts') / $limit, 0);

    $posts = selectAllFromPostsWithUsersOnIndex('posts', 'users', $limit, $offset);
    $topTopic = selectTopTopicFromPostsOnIndex('posts');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/scss/style.css">
    <title>Pirania</title>
</head>
<body>

<?php include("app/include/header.php"); ?>

<main>
    <h2 class="visually-hidden">welcome-pirania</h2>
    <div class="container">
        <div class="container-fluid">
            <div class="welcome-pirania__wrapper">
                <img src="assets/img/welcome-pirania.jpg" alt="Hello" class="welcome-piraniaimage">
                <div class="welcome-pirania__wrapper-text">
                    <h2 class="welcome-pirania__heading">Добро пожаловать в клуб рыбака</h2>
                    <p class="welcome-pirania__slogan">Общение происходит у нас на форуме</p>
                </div>
                <div class="welcome-pirania__links-wrapper">
                    <a href="#" class="welcome-pirania__link">Войти на форум</a>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($_SESSION['id'])): ?>
    <div class="container">
        <div class="content row">
            <div class="main-content col-12 col-md-9">
                <h2>Последнии публикации</h2>
                <?php foreach ($posts as $post): ?>
                <div class="post row">
                    <div class="img col-12 col-md-4">
                        <img src="<?=BASE_URL . 'assets/images/posts/' . $post['img'] ?>" alt="<?=$post['title']?>" class="img-thumbnail">
                    </div>
                    <div class="post_text col-12 col-md-8">
                        <h2>
                        <?php if (strlen($post['title'])< 30):?> 
                            <a href="<?=BASE_URL . 'single.php?post=' . $post['id']; ?>"> <?=$post['title']; ?></a>
                        <?else: ?>
                            <a href="<?=BASE_URL . 'single.php?post=' . $post['id']; ?>"> <?=mb_substr($post['title'],0,30). '...'; ?></a>
                        <?endif; ?>
                        </h2>
                        <i class="fas"><?='Автор: '. $post['username']; ?> ||</i>
                        <i class="fas"><?='Дата: '. date("m.d.y H:i", strtotime($post['created_date'])); ?></i>
                        <?php if (strlen($post['content'])< 150):?> 
                            <?=$post['content']; ?>
                        <?else: ?>
                            <?=mb_substr($post['content'],0,150). '...'; ?>
                        <?endif; ?>
                        
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="slidebar col-12 col-md-3">
                <div class="section search">
                    <h3>Поиск</h3>
                    <form action="<?=BASE_URL . 'search.php'?>" method="post">
                        <input type="text" name="search-item" placeholder="Поиск..." class="text-input">
                    </form>
                </div>
                <div class="section topics">
                    <h3>Категории</h3>
                    <ul>
                        <?php foreach($topics as $key => $topic): ?>
                        <li><a href="<?=BASE_URL . 'category.php?id=' . $topic['id']; ?>"><?=$topic['name'];?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </topics>
            </div>
        </div>
    </div>
    <?php else: ?>
        <div class="container">
        <div class="content row">
            <div class="main-content col-12 mb-5 mt-5 text-center ">
                <h2>Для полного доступа к сайту необходимо авторизироваться!</h2>
    <?php endif; ?>
</main>
<?php include("app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
