<header class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <a href="<?php echo BASE_URL ?>" class="logo">
                        <img src="../../assets/img/logo.webp" alt="Pirania" class="logo__img">
                    </a>
                </div>
                <div class="col-8">
                    <nav>
                        <ul class="navigation__list">
                            <li class="navigation__item">
                                <a href="<?php echo BASE_URL ?>" class="link" >Блог</a>
                            </li>
                            <li class="navigation__item">
                                <a href="<?php echo BASE_URL ?>" class="link" >Документация</a>
                            </li>
                            <li class="navigation__item">
                                <a href="<?php echo BASE_URL ?>"  class="link">Форум</a>
                            </li>
                            <li class="navigation__item">
                                <a href="<?php echo BASE_URL ?>" class="link">
                                    <?php echo $_SESSION['login']; ?>
                                </a>
                                <ul>
                                    <li><a href="<?php echo BASE_URL . 'logout.php'; ?>" class="link link--nav">Выход</a></li>
                                </ul>
                            </li>
                            <li class="navigation__item">
                                <a href="<?php echo BASE_URL ?>" class="link">MD/RU</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>