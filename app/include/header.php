<header class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <a href="<?php echo BASE_URL ?>" class="logo">
                        <img src="assets/img/logo.webp" alt="Pirania" class="logo__img">
                    </a>
                </div>
                <div class="col-8">
                    <nav>
                        <?php if (isset($_SESSION['id'])): ?>
                            <ul class="navigation__list">
                                <li class="navigation__item">
                                        <a href="blogs.html" class="link" >Блог</a>
                                </li>
                                <li class="navigation__item">
                                    <a href="docs.html" class="link" >Документация</a>
                                </li>
                                <li class="navigation__item">
                                    <a href="contacts.html" class="link" >Контакты</a>
                                </li>
                                <li class="navigation__item">
                                    <a href="#"  class="link">Форум</a>
                                </li>
                                <li class="navigation__item">
                                    <?php if (isset($_SESSION['id'])): ?>
                                    <a href="<?php echo BASE_URL ?>" class="link">
                                        <?php echo $_SESSION['login']; ?>
                                    </a>
                                    <ul>
                                        <?php if ($_SESSION['admin']): ?>
                                        <li><a href="<?php echo BASE_URL . 'admin/posts/index.php'; ?>" class="link link--nav">Кабинет</a></li>
                                        <?php endif; ?>
                                        <li><a href="<?php echo BASE_URL . 'logout.php'; ?>" class="link link--nav">Выход</a></li>
                                    </ul>
                                    <?php else: ?>
                                    <a href="<?php echo BASE_URL . 'log.php'; ?>" class="link">
                                        Авторизироваться
                                    </a>
                                    <ul>
                                        <li><a href="<?php echo BASE_URL . 'reg.php'; ?>" class="link link--nav">Регистрация</a></li>
                                    </ul>
                                    <?php endif; ?>
                                </li>
                                <li class="navigation__item">
                                    <a href="#" class="link">MD/RU</a>
                                </li>
                            </ul>
                        <?php else: ?>
                            <ul class="navigation__list">
                                <li class="navigation__item"><a href="<?php echo BASE_URL . 'log.php'; ?>" class="link"> Авторизироваться</a></li>
                                <li class="navigation__item"><a href="<?php echo BASE_URL . 'reg.php'; ?>" class="link link--nav">Регистрация</a></li>
                                <li class="navigation__item"><a href="#" class="link">MD/RU</a></li>
                            </ul>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>