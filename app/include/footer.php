
<div class="footer container-fluid">
    <div class="footer-content container">
        <div class="row">
            <div class="footer-section about col-md-4 col-12">
                <h3 class="logo-text">Pirania</h3>
                <p>Pirania-Клуб рыбаков РМ</p>
                <div class="contact">
                    <span><i class="fas fa-phone"></i>&nbsp;+37369947773</span>
                    <span><i class="fas fa-envelope"></i>&nbsp;pirania@md.ru</span>
                </div>
                <div class="socials">
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="footer-section links col-md-4 col-12">
                <h3>Навигация</h3>
                <ul>
                    <li><a href="#">Блог</a></li>
                    <li><a href="#">Форум</a></li>
                    <li><a href="#">Контакты</a></li>
                    <li><a href="#">Документация</a></li>
                </ul>
            </div>
            <div class="footer-section contact-form col-md-4 col-12">
                <h3>Связаться</h3>
                <form action="index.html" method="post">
                    <input type="email" name="email" class="text-input contact-input" placeholder="Ваша почта...">
                    <textarea name="message" cols="30" rows="4" class="text-input contact-input" placeholder="Ваше сообщение"></textarea>
                    <button type="submit" class="btn btn-big contact-btn"><i class="fas fa-envelope"></i>Отправить</button>
                </form>
            </div>
            <div class="footer-bottom">
                &copy; pirania.md | All rights
            </div>
        </div>
    </div>
</div>
</footer>