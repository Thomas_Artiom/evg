<?php 
include SITE_ROOT . "/app/database/db.php";

$errMsg = [];

function userAuth($user){
    $_SESSION['id'] = $user['id'];
    $_SESSION['login'] = $user['username'];
    $_SESSION['admin'] = $user['admin'];
    if($_SESSION['admin']){
        header('location:' . BASE_URL . 'admin/posts/index.php');
    } else {
        header('location: ' . BASE_URL);
    }
}

$users = selectAll('users');

//Регастрация пользователя просто
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-reg'])){
    $admin = 0;
    $login = trim($_POST['login']);
    $mail = trim($_POST['email']);
    $passF = trim($_POST['password-f']);
    $passS = trim($_POST['password-s']);

    if ($login === '' || $mail === '' || $passF === ''){
        array_push($errMsg,"Заполните все поля!");
    } elseif (mb_strlen($login, 'UTF8') <2 ){
        array_push($errMsg,"Логин слишком короткий!(не менее 2-х символов)");
    } elseif ($passF !== $passS){
        array_push($errMsg,"Введите одинаковые пароли!");
    }
    else {
        $existence = selectOne('users', ['email' => $mail]);
        if (!empty($existence['email']) && $existence['email'] === $mail){
            array_push($errMsg,"Пользователь с данной почтой уже существует!");
        } 
        else{
            $pass = password_hash($passF, PASSWORD_DEFAULT);
            $post = [
                'admin' => $admin,
                'username' => $login,
                'email' => $mail,
                'password' => $pass
                ];
            $id = insert('users', $post);
            $user = selectOne('users', ['id' => $id]);
            
            userAuth($user);
            
        }
    }
} else {
    $login = '';
    $email = '';
}
//Авторазиция
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['button-log'])){
    $mail = trim($_POST['email']);
    $pass = trim($_POST['password']);

    if ($mail === '' || $pass === ''){
        array_push($errMsg,"Заполните все поля!");
    } else {
        $existence = selectOne('users', ['email' => $mail]);
        if($existence && password_verify($pass, $existence['password'])){
            userAuth($existence);
        } else {
            array_push($errMsg,"Неправильный емайл и пароль!");
        }
        
    }
} else {
    $mail = '';
};


// регистрация в админке
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['create_user'])){
    
    
    $admin = 0;
    $login = trim($_POST['login']);
    $mail = trim($_POST['email']);
    $passF = trim($_POST['password-f']);
    $passS = trim($_POST['password-s']);

    if ($login === '' || $mail === '' || $passF === ''){
        array_push($errMsg,"Заполните все поля!");
    } elseif (mb_strlen($login, 'UTF8') <2 ){
        array_push($errMsg,"Логин слишком короткий!(не менее 2-х символов)");
    } elseif ($passF !== $passS){
        array_push($errMsg,"Введите одинаковые пароли!");
    }
    else {
        $existence = selectOne('users', ['email' => $mail]);
        if (!empty($existence['email']) && $existence['email'] === $mail){
            array_push($errMsg,"Пользователь с данной почтой уже существует!");
        } 
        else{
            $pass = password_hash($passF, PASSWORD_DEFAULT);
            if (isset($_POST['admin'])) $admin = 1;
            $user = [
                'admin' => $admin,
                'username' => $login,
                'email' => $mail,
                'password' => $pass
                ];
            $id = insert('users', $user);
            $user = selectOne('users', ['id' => $id]);
            header('location:' . BASE_URL . 'admin/users/index.php');
        }
    }
} else {
    $login = '';
    $mail = '';
}


// РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЯ ЧЕРЕЗ АДМИНКУ
if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['edit_id'])){
    $user = selectOne('users', ['id' => $_GET['edit_id']]);

    $id =  $user['id'];
    $admin =  $user['admin'];
    $username = $user['username'];
    $email = $user['email'];
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['update-user'])){

    $id = $_POST['id'];
    $mail = trim($_POST['mail']);
    $login = trim($_POST['login']);
    $passF = trim($_POST['pass-first']);
    $passS = trim($_POST['pass-second']);
    $admin = isset($_POST['admin']) ? 1 : 0;

    if($login === ''){
        array_push($errMsg, "Не все поля заполнены!");
    }elseif (mb_strlen($login, 'UTF8') < 2){
        array_push($errMsg, "Логин должен быть более 2-х символов");
    }elseif ($passF !== $passS) {
        array_push($errMsg, "Пароли в обеих полях должны соответствовать!");
    }else{
        $pass = password_hash($passF, PASSWORD_DEFAULT);
        if (isset($_POST['admin'])) $admin = 1;
        $user = [
            'admin' => $admin,
            'username' => $login,
//            'email' => $mail,
            'password' => $pass
        ];

        $user = update('users', $id, $user);
        header('location: ' . BASE_URL . 'admin/users/index.php');
    }
}
//else{
//     $id =  $user['id'];
//     $admin =  $user['admin'];
//     $username = $user['username'];
//     $email = $user['email'];
// }


//Удал
if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['delete_id'])){
    $id = $_GET['delete_id'];
    delete('users',$id);
    header('location:' . BASE_URL . 'admin/users/index.php');
}