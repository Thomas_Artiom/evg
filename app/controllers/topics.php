<?php 

include SITE_ROOT . "/app/database/db.php";
$errMsg = [];
$id = '';
$name = '';
$description = '';
$topics = selectAll('topics');
//регистрация
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['topic-create'])){

    $name = trim($_POST['name']);
    $description = trim($_POST['description']);

    if ($name === '' || $description === ''){
        array_push($errMsg,"Заполните все поля!");
    } elseif (mb_strlen($name, 'UTF8') <2 ){
        array_push($errMsg,"Категория слишком короткая!(не менее 2-х символов)");
    }
    else {
        $existence = selectOne('topics', ['name' => $name]);
        if (!empty($existence['name']) && $existence['name'] === $name){
            array_push($errMsg,"Категория уже существует!");
        } 
        else{
            $topic = [
                'name' => $name,
                'description' => $description
                ];
            $id = insert('topics', $topic);
            $topic = selectOne('topics', ['id' => $id]);
            header('location:' . BASE_URL . 'admin/topics/index.php');
        }
    }
} else {
    $name = '';
    $description = '';
}

//Редактирование категорий
if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id'])){
    $id = $_GET['id'];
    $topic = selectOne('topics', ['id' => $id]);
    $id = $topic['id'];
    $name = $topic['name'];
    $description = $topic['description'];
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['topic-edit'])){

    $name = trim($_POST['name']);
    $description = trim($_POST['description']);

    if ($name === '' || $description === ''){
        array_push($errMsg,"Заполните все поля!");
    } elseif (mb_strlen($name, 'UTF8') <2 ){
        array_push($errMsg,"Категория слишком короткая!(не менее 2-х символов)");
    }
    else {
            $topic = [
                'name' => $name,
                'description' => $description
                ];
            $id = $_POST['id'];
            $topic_id = update('topics', $id, $topic);
            header('location:' . BASE_URL . 'admin/topics/index.php');
    }
}


//Удаление

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['del_id'])){
    $id = $_GET['del_id'];
    delete('topics',$id);
    array_push($errMsg,"Категория $name удалена");
    header('location:' . BASE_URL . 'admin/topics/index.php');
}
?>