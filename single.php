<?php include("path.php"); 
include "app/controllers/topics.php";
$post = selectPostFromPostsWithUsersOnSingle('posts', 'users', $_GET['post']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>
<?php include("app/include/header.php"); ?>
<main>
<?php if (isset($_SESSION['id'])): ?>
    <div class="container">
        <div class="content row">
            <div class="main-content col-12 col-md-9">
                <h2><?php echo $post['title']; ?></h2>

                <div class="single_post row">
                    <div class="img col-12">
                        <img src="<?=BASE_URL . 'assets/images/posts/' . $post['img'] ?>" alt="<?=$post['title']?>" class="img-thumbnail">
                    </div>
                    <div class="single_post_text col-12">
                        <i class="fas"><?='Автор: '. $post['username']; ?> ||</i>
                        <i class="fas"><?='Дата: '. date("m.d.y H:i", strtotime($post['created_date'])); ?></i>
                        <?=$post['content']; ?>
                    </div>
                    <div class="single_post_text col-4">
                    <p>Файл:</p>
                        <a href="<?=BASE_URL . 'assets/images/posts/pdf/' . $post['pdf'] ?>"></a>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    <?php else: ?>
        <div class="container">
        <div class="content row">
            <div class="main-content col-12 mb-5 mt-5 text-center ">
                <h2>Для полного доступа к сайту необходимо авторизироваться!</h2>
    <?php endif; ?>

    <?php include("app/include/footer.php"); ?>
</main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
