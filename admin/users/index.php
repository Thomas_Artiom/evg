
<?php 
    include '../../path.php'; 
    include "../../app/controllers/users.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>

<?php include '../../app/include/header-admin.php'; ?>
<main>
<div class="container">
<?php include '../../app/include/sidebar-admin.php'; ?>
        <div class="posts col-8">
            <div class="row buttons">
                <a href="<?php echo BASE_URL . 'admin/users/create.php' ;?>" class="col-2 btn btn-success">Создать пользователя</a>
                <span class="col-1"></span>
                <a href="<?php echo BASE_URL . 'admin/users/index.php' ;?>" class="col-3 btn btn-warning">Управление пользователем</a>
            </div>
            <div class="row title-table">
                <h2>Пользователи</h2>
                <div class="col-1">ID</div>
                <div class="col-2">Логин</div>
                <div class="col-3">Почта</div>
                <div class="col-2">Роль</div>
                <div class="col-4">Управление</div>
            </div>
            <?php foreach ($users as $key => $user): ?>
            <div class="row post">
                <div class="col-1"><?=$user['id'];?></div>
                <div class="col-2"><?=$user['username'];?></div>
                <div class="col-3"><?=$user['email'];?></div>
                <?php if ($user['admin'] ==1): ?>
                    <div class="col-2">Admin</div>
                <?php else: ?>
                    <div class="col-2">User</div>
                <?php endif; ?>
                <div class="red col-2"><a href="edit.php?edit_id=<?=$user['id'];?>">Edit</a></div>
                <div class="del col-2"><a href="index.php?delete_id=<?=$user['id'];?>">Delete</a></div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</main>
<?php include("../../app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
