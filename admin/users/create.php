
<?php 
    include '../../path.php'; 
    include "../../app/controllers/users.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>

<?php include '../../app/include/header-admin.php'; ?>
<main>
<div class="container">
<?php include '../../app/include/sidebar-admin.php'; ?>
        <div class="posts col-8">
            <div class="row buttons">
                <a href="<?php echo BASE_URL . 'admin/users/create.php' ;?>" class="col-2 btn btn-success">Добавить пользователя</a>
                <span class="col-1"></span>
                <a href="<?php echo BASE_URL . 'admin/users/index.php' ;?>" class="col-3 btn btn-warning">Управление пользователем</a>
            </div>
            <div class="row title-table">
                <h2>Добавление пользователя</h2>
            </div>
            <div class="row add-post">
                <form action="create.php" method="post">
                    <div class="col md-4 err">
                        <?php include '../../app/helps/errorInfo.php'; ?>
                    </div>
                    <div class="col labels mb-4">
                        <label for="formGroupExampleInput" class="form-label">Логин</label>
                        <input name="login" type="text" valuse="<?=$login?>" class="form-control" id="formGroupExampleInput" placeholder="Введите ваш логин...">
                    </div>
                    <div class="col labels mb-4">
                        <label for="exampleInputEmail1" class="form-label">Ваша почта</label>
                        <input name="email" type="email" valuse="<?=$mail?>" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите вашу почту...">
                    </div>
                    <div class="col labels mb-4">
                        <label for="exampleInputPassword1" class="form-label">Введите пароль</label>
                        <input name="password-f" type="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <div class="col labels mb-4">
                        <label for="exampleInputPassword2" class="form-label">Повторите введенный пароль</label>
                        <input name="password-s" type="password" class="form-control" id="exampleInputPassword2">
                    </div>
                    <div class="form-check mb-2">
                        <input name="admin" value="1" class="form-check-input" type="checkbox" id="flexCheckChecked">
                        <label class="form-check-label" for="flexCheckChecked">
                            Администратор?
                        </label>
                    </div>
                    <div class="col mb-4">
                        <button name="create_user" class="btn btn-primary" type="submit">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</main>
<?php include("../../app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    
</body>
</html>
