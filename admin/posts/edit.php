<?php 
    include '../../path.php';
    include "../../app/controllers/posts.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>
<?php include '../../app/include/header-admin.php'; ?>
<main>
<div class="container">
    <?php include '../../app/include/sidebar-admin.php'; ?>
        <div class="posts col-8">
            <div class="row title-table">
                <h2>Редактирование записи</h2>
            </div>
            <div class="row add-post">
                <form action="edit.php" method="post" enctype="multipart/form-data">
                    <div class="col md-4 err">
                        <?php include '../../app/helps/errorInfo.php'; ?>
                    </div>
                    <input type="hidden" name="id" value="<?=$id?>">
                    <div class="col mb-4">
                        <input value="<?=$post['title'];?>" name="title" type="text" class="form-control" placeholder="Title" aria-label="Название статьи">
                    </div>
                    <div class="col mb-4">
                        <label for="editor" class="form-label">Содержимое записи</label>
                        <textarea name="content" id="editor" class="form-control" rows="6"><?=$post['content'];?></textarea>
                    </div>
                    <div class="input-group col mb-4">
                        <input name="img" type="file" class="form-control" id="inputGroupFile01">
                        <label class="input-group-text" for="inputGroupFile01">Загрузить
                        </label>
                    </div> 
                    <select name="topic" class="form-select mb-4" aria-label="Default select example">
                        <?php foreach($topics as $key => $topic): ?>
                            <option value="<?=$topic['id'];?>"><?=$topic['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="form-check mb-2">
                        <?php if (empty($publish) && $publish == 0): ?>
                            <input name="publish"class="form-check-input" type="checkbox" id="flexCheckChecked">
                            <label class="form-check-label" for="flexCheckChecked">
                                Опубликовать
                            </label>
                        <?php else: ?>
                            <input name="publish"class="form-check-input" type="checkbox" id="flexCheckChecked" checked>
                            <label class="form-check-label" for="flexCheckChecked">
                                Опубликовать
                            </label>
                        <?php endif; ?>
                    </div>
                    <div class="col mb-4">
                        <button name="edit_post" class="btn btn-primary" type="submit">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</main>
<?php include("../../app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>
    <script src="../../assets/js/script.js"></script>
</body>
</html>
