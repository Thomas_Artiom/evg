<?php 
    include '../../path.php'; 
    include "../../app/controllers/posts.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>

<?php include '../../app/include/header-admin.php'; ?>
<main>
<div class="container">
<?php include '../../app/include/sidebar-admin.php'; ?>
        <div class="posts col-8">
            <div class="row buttons">
                <a href="<?php echo BASE_URL . 'admin/posts/create.php' ;?>" class="col-2 btn btn-success">Создать запись</a>
                <span class="col-1"></span>
                <a href="<?php echo BASE_URL . 'admin/posts/index.php' ;?>" class="col-3 btn btn-warning">Управление записями</a>
            </div>
            <div class="row title-table">
                <h2>Управление</h2>
                <div class="col-1">ID</div>
                <div class="col-5">Название</div>
                <div class="col-2">Автор(id)</div>
                <div class="col-4">Управление</div>
            </div>
            <?php foreach ($postsAdm as $key => $post): ?>
            <div class="row post">
                <div class="id col-1"><?=$key+1;?></div>
                <div class="title col-5">
                        <?php if (strlen($post['title'])< 20):?> 
                            <a href="#"> <?=$post['title']; ?></a>
                        <?else: ?>
                            <a href="#"> <?=mb_substr($post['title'],0,19). '...'; ?></a>
                        <?endif; ?></div>
                <div class="author col-2"><?=$post['username'];?></div>
                <div class="red col-1"><a href="edit.php?id=<?=$post['id'];?>">Edit</a></div>
                <div class="del col-1"><a href="edit.php?delete_id=<?=$post['id'];?>">Delete</a></div>
                <?php if ($post['status']): ?>
                    <div class="status col-2"><a href="edit.php?publish=0&pub_id=<?=$post['id'];?>">Запостить</a></div>
                <?php else: ?>
                    <div class="status col-2"><a href="edit.php?publish=1&pub_id=<?=$post['id'];?>">Разпостить</a></div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</main>
<?php include("../../app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
