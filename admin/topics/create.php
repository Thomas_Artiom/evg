
<?php 
    include '../../path.php';
    include "../../app/controllers/topics.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <title>Pirania</title>
</head>
<body>

<?php include '../../app/include/header-admin.php'; ?>
<main>
<div class="container">
<?php include '../../app/include/sidebar-admin.php'; ?>
        <div class="posts col-8">
        
        <div class="row buttons">
                <a href="<?php echo BASE_URL . 'admin/topics/create.php' ;?>" class="col-2 btn btn-success">Создать категорию</a>
                <span class="col-1"></span>
                <a href="<?php echo BASE_URL . 'admin/topics/index.php' ;?>" class="col-3 btn btn-warning">Управление категориями</a>
            </div>
            <div class="row title-table">
                <h2>Добавление категории</h2>
            </div>
            
            <div class="row add-post">
                <form action="create.php" method="post">
                    <div class="col md-4 err">
                        <?php include '../../app/helps/errorInfo.php'; ?>
                    </div>
                    <div class="col mb-4">
                        <input name="name" value="<?=$name?>" type="text" class="form-control" placeholder="Title" aria-label="Название категории">
                    </div>
                    <div class="col mb-4">
                        <label for="content" class="form-label">Описание категории</label>
                        <textarea name="description" class="form-control" id="content" rows="6"></textarea>
                    </div>
                    <div class="col mb-4">
                        <button name="topic-create" class="btn btn-primary" type="submit">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</main>
<?php include("../../app/include/footer.php"); ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/32.0.0/classic/ckeditor.js"></script>
    <script src="../../assets/js/script.js"></script>
</body>
</html>
